import { UserRole } from '../model/User'
import { Authenticator } from './Authenticator'

export class RoleValidator {
  checkIfIsAdminRole(token: string): boolean {
    const authenticator = new Authenticator()
    const role = authenticator.getData(token)

    if (role === UserRole.USER) {
      return false
    } else {
      return true
    }
  }
}
