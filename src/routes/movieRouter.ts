/* eslint-disable @typescript-eslint/no-misused-promises */
import express from 'express'
import { MovieController } from '../controller/MovieController'

export const movieRouter = express.Router()

const movieController = new MovieController()

movieRouter.post('/', movieController.addMovie)
movieRouter.patch('/:id', movieController.rateMovie)
movieRouter.get('/', movieController.getMovies)
movieRouter.get('/:id', movieController.getMovieById)
