/* eslint-disable @typescript-eslint/no-misused-promises */
import express from 'express'
import { UserController } from '../controller/UserController'

export const userRouter = express.Router()

const userController = new UserController()

userRouter.post('/', userController.signup)
userRouter.post('/login', userController.login)
userRouter.patch('/:id', userController.softDelete)
userRouter.patch('/edit/:id', userController.editUser)
