import { InvalidParameterError } from '../errors/InvalidParameterError'
import { IdGenerator } from '../services/IdGenerator'
import { NotFoundError } from '../errors/NotFoundError'
import { MovieDatabase } from '../data/MovieDatabase'
import { Movie, MovieInputDTO, MovieSearchFilterDTO } from '../model/Movie'

export class MovieBusiness {
  async addMovie(movie: MovieInputDTO): Promise<void> {
    const movieDatabase = new MovieDatabase()
    if (!movie.title || !movie.director || !movie.synopsis) {
      throw new InvalidParameterError('Title, director and synopsis are mandatory fields')
    }

    const idGenerator = new IdGenerator()
    const id = idGenerator.generate()

    await movieDatabase.createMovie(
      id,
      movie.title,
      movie.director,
      movie.synopsis
    )
  }

  async getMovies(filter?: MovieSearchFilterDTO): Promise<Movie> {
    const movieDatabase = new MovieDatabase()
    const movies = await movieDatabase.getMovies(filter)
    return movies
  }

  async rateMovie(rating: number, id: string): Promise<void> {
    const movieDatabase = new MovieDatabase()
    const movieToRate = await movieDatabase.getMovieById(id)

    let newRating

    if (!movieToRate) {
      throw new NotFoundError('Movie to rate not found, check id provided')
    }

    if (rating < 0 || rating > 4) {
      throw new InvalidParameterError('Rating must be a number between 0 and 4')
    }

    if (movieToRate.getRating === null) {
      newRating = rating
    }

    const newRatingsMade = Number(movieToRate.getRatingsMade()) + 1
    newRating = (Number(movieToRate.getRating()) + Number(rating)) / Number(newRatingsMade)

    await movieDatabase.updateRating({ rating: newRating, ratingsMade: newRatingsMade }, id)
  }

  async getMovieDetailById(id: string): Promise<Movie> {
    const movieDatabase = new MovieDatabase()

    if (!id) {
      throw new InvalidParameterError('Movie Id not provided')
    }

    const movie = await movieDatabase.getMovieById(id)

    if (!movie) {
      throw new NotFoundError('Movie not found')
    }

    return movie
  }
}
