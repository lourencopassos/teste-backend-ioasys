export class Movie {
  constructor(
    private id: string,
    private title: string,
    private synopsis: string,
    private director: string,
    private rating: null | number = null,
    private ratingsMade: number = 0
  ) { }

  getId(): string {
    return this.id
  }

  getTitle(): string {
    return this.title
  }

  getDirector(): string {
    return this.director
  }

  getRating(): number | null {
    return this.rating
  }

  getRatingsMade(): number {
    return this.ratingsMade
  }

  getSynopsis(): string {
    return this.synopsis
  }

  setId(id: string): void {
    this.id = id
  }

  setTitle(title: string): void {
    this.title = title
  }

  setDirector(director: string): void {
    this.director = director
  }

  setSynopsis(synopsis: string): void {
    this.synopsis = synopsis
  }

  setRating(rating: number | null): void {
    this.rating = rating
  }

  setRatingsMade(ratingsMade: number): void {
    this.ratingsMade = ratingsMade
  }

  static toMovieModel(movie: any): Movie {
    return new Movie(movie.id, movie.title, movie.synopsis, movie.director, movie.rating, movie.ratingsMade)
  }
}

export interface MovieInputDTO {
  title: string
  synopsis: string
  director: string
}

export interface MovieUpdateRatingDTO {
  rating: number
  ratingsMade: number
}

export interface MovieEditDTO {
  available?: boolean
  title?: string
  synopsis?: string
  director?: string
}

export interface MovieSearchFilterDTO {
  available?: boolean
  title?: string
  synopsis?: string
  director?: string
}
