export class User {
  constructor(
    private id: string,
    private email: string,
    private password: string,
    private role: UserRole,
    private status: Status = Status.ENABLED
  ) { }

  getId(): string {
    return this.id
  }

  getEmail(): string {
    return this.email
  }

  getPassword(): string {
    return this.password
  }

  getRole(): UserRole {
    return this.role
  }

  getStatus(): Status {
    return this.status
  }

  setId(id: string): void {
    this.id = id
  }

  setEmail(email: string): void {
    this.email = email
  }

  setPassword(password: string): void {
    this.password = password
  }

  setUserRole(userRole: string): void {
    const roleStringToUserRole = stringToRole(userRole)
    this.role = roleStringToUserRole
  }

  setStatus(status: string): void {
    const statusStringToStatus = stringToStatus(status)
    this.status = statusStringToStatus
  }

  static toUserModel(user: any): User {
    return new User(user.id, user.name, user.password, user.role, user.active)
  }
}

export interface UserInputDTO {
  email: string
  password: string
  role: UserRole
}

export interface UserEditDTO {
  email?: string
  password?: string
  role?: UserRole
}

export interface LoginInputDTO {
  email: string
  password: string
}

export enum UserRole {
  ADMIN = 'admin',
  USER = 'user',
}

export enum Status {
  ENABLED = 'enabled',
  DISABLED = 'disabled'
}

export const stringToRole = (input: string): UserRole => {
  switch (input) {
    case 'admin':
      return UserRole.ADMIN
    case 'user':
      return UserRole.USER
    default:
      throw new Error('Invalid role')
  }
}

export const stringToStatus = (input: string): Status => {
  switch (input) {
    case 'enabled':
      return Status.ENABLED
    case 'disabled':
      return Status.DISABLED
    default:
      throw new Error('Invalid status')
  }
}
