import { Request, Response } from 'express'
import { UserInputDTO, LoginInputDTO } from '../model/User'
import { UserBusiness } from '../business/UserBusiness'
import { BaseDatabase } from '../data/BaseDatabase'
import { UserDatabase } from '../data/UserDatabase'
import { RoleValidator } from '../services/RoleValidator'

export class UserController {
  async signup(req: Request, res: Response): Promise<void> {
    try {
      const input: UserInputDTO = {
        email: req.body.email,
        password: req.body.password,
        role: req.body.role
      }

      const userBusiness = new UserBusiness()
      await userBusiness.createUser(input)

      res.status(201).send({ message: 'Sucess' })
    } catch (error) {
      res.status(400).send({ error: error.message })
    } finally {
      await BaseDatabase.destroyConnection()
    }
  }

  async login(req: Request, res: Response): Promise<void> {
    try {
      const loginData: LoginInputDTO = {
        email: req.body.email,
        password: req.body.password
      }

      const userBusiness = new UserBusiness()
      const token = await userBusiness.getUserByEmail(loginData)

      res.status(200).send({ token })
    } catch (error) {
      res.status(400).send({ error: error.message })
    } finally {
      await BaseDatabase.destroyConnection()
    }
  }

  async softDelete(req: Request, res: Response): Promise<any> {
    try {
      const token: any = req.headers.authorization

      if (!token) {
        return res.status(401).send({ error: 'Unauthorized' })
      }

      const roleValidator = new RoleValidator()
      const admin = roleValidator.checkIfIsAdminRole(token)

      if (!admin) {
        return res.status(403).send({
          error:
            'Unauthorized, only admin has permission to delete users '
        })
      }

      const id = req.params.id

      const userDatabase = new UserDatabase()
      const userInDatabase = userDatabase.getUserById(id)

      if (userInDatabase === undefined) {
        res.status(400).send({ error: 'User to delete not found, check id' })
      }

      const userBusiness = new UserBusiness()
      await userBusiness.softDeleteUserById(id)

      res.status(204).send({ message: 'Sucess' })
    } catch (error) {
      res.status(400).send({ error: error.message })
    } finally {
      await BaseDatabase.destroyConnection()
    }
  }

  async editUser(req: Request, res: Response): Promise<any> {
    try {
      const token: any = req.headers.authorization

      const id = req.params.id

      const roleValidator = new RoleValidator()
      const admin = roleValidator.checkIfIsAdminRole(token)

      if (!admin) {
        return res.status(403).send({
          error:
            'Unauthorized, only admin has permission to delete users '
        })
      }

      const userDatabase = new UserDatabase()
      const userInDatabase = userDatabase.getUserById(id)

      if (userInDatabase === undefined) {
        res.status(400).send({ error: 'User to edit not found, check id' })
      }

      const editInformations = req.body

      const userBusiness = new UserBusiness()
      await userBusiness.editUserById(id, editInformations)
      res.status(200).send({ message: 'Sucess' })
    } catch (error) {
      res.status(400).send({ error: error.message })
    } finally {
      await BaseDatabase.destroyConnection()
    }
  }
}
