import { Request, Response } from 'express'
import { MovieBusiness } from '../business/MovieBusiness'
import { BaseDatabase } from '../data/BaseDatabase'
import { RoleValidator } from '../services/RoleValidator'
import { MovieInputDTO } from '../model/Movie'

export class MovieController {
  async addMovie(req: Request, res: Response): Promise<any> {
    try {
      const input: MovieInputDTO = {
        title: req.body.title,
        director: req.body.director,
        synopsis: req.body.synopsis
      }

      const token: any = req.headers.authorization

      if (!token) {
        return res.status(401).send({ error: 'Unauthorized' })
      }

      const roleValidator = new RoleValidator()
      const admin = roleValidator.checkIfIsAdminRole(token)

      if (!admin) {
        return res.status(403).send({
          error:
            'Unauthorized, only admin has permission to delete users '
        })
      }

      const movieBusiness = new MovieBusiness()
      await movieBusiness.addMovie(input)

      res.status(201).send({ message: 'Sucess' })
    } catch (error) {
      res.status(400).send({ error: error.message })
    } finally {
      await BaseDatabase.destroyConnection()
    }
  }

  async rateMovie(req: Request, res: Response): Promise<any> {
    try {
      const { rating } = req.body

      const token: any = req.headers.authorization
      const id = req.params.id

      if (!token) {
        return res.status(401).send({ error: 'Unauthorized' })
      }

      const roleValidator = new RoleValidator()
      const admin = roleValidator.checkIfIsAdminRole(token)

      if (admin) {
        return res.status(403).send({
          error:
            'Unauthorized, only user has permission to rate movies '
        })
      }
      const movieBusiness = new MovieBusiness()
      await movieBusiness.rateMovie(rating, id)

      res.status(200).send({ message: 'Sucess' })
    } catch (error) {
      res.status(400).send({ error: error.message })
    } finally {
      await BaseDatabase.destroyConnection()
    }
  }

  async getMovies(req: Request, res: Response): Promise<any> {
    try {
      let movies
      const token: any = req.headers.authorization

      if (!token) {
        return res.status(401).send({ error: 'Unauthorized' })
      }

      const filter = req.body
      const movieBusiness = new MovieBusiness()

      if (filter) {
        movies = await movieBusiness.getMovies(filter)
      }

      if (!filter) {
        movies = await movieBusiness.getMovies()
      }

      res.status(200).send(movies)
    } catch (error) {
      res.status(400).send({ error: error.message })
    } finally {
      await BaseDatabase.destroyConnection()
    }
  }

  async getMovieById(req: Request, res: Response): Promise<any> {
    try {
      const token: any = req.headers.authorization

      if (!token) {
        return res.status(401).send({ error: 'Unauthorized' })
      }

      const id = req.params.id

      const movieBusiness = new MovieBusiness()
      const movie = await movieBusiness.getMovieDetailById(id)

      if (movie === undefined) {
        res.status(404).send({ error: 'Movie not Found' })
      }

      res.status(200).send(movie)
    } catch (error) {
      res.status(400).send({ error: error.message })
    } finally {
      await BaseDatabase.destroyConnection()
    }
  }
}
