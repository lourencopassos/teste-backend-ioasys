import { BaseDatabase } from './BaseDatabase'
import { Movie, MovieEditDTO, MovieSearchFilterDTO, MovieUpdateRatingDTO } from '../model/Movie'

export class MovieDatabase extends BaseDatabase {
  private static readonly TABLE_NAME = 'IoasysMovies'

  public async createMovie(
    id: string,
    title: string,
    synopsis: string,
    director: string
  ): Promise<void> {
    try {
      await this.getConnection()
        .insert({
          id,
          title,
          synopsis,
          director
        })
        .into(MovieDatabase.TABLE_NAME)
    } catch (error) {
      throw new Error(error.sqlMessage || error.message)
    }
  }

  public async getMovieByTitle(title: string): Promise<Movie> {
    try {
      const result = await this.getConnection()
        .select('*')
        .from(MovieDatabase.TABLE_NAME)
        .where({ title })

      return Movie.toMovieModel(result[0])
    } catch (error) {
      throw new Error(error.sqlMessage || error.message)
    }
  }

  public async getMovieById(id: string): Promise<Movie> {
    try {
      const result = await this.getConnection()
        .select('*')
        .from(MovieDatabase.TABLE_NAME)
        .where({ id })
      return Movie.toMovieModel(result[0])
    } catch (error) {
      throw new Error(error.sqlMessage || error.message)
    }
  }

  public async updateRating(movieRatingObject: MovieUpdateRatingDTO, id: string): Promise<void> {
    try {
      await this.getConnection()
        .select('*')
        .update({ rating: movieRatingObject.rating, ratingsMade: movieRatingObject.ratingsMade })
        .from(MovieDatabase.TABLE_NAME)
        .where({ id })
    } catch (error) {
      throw new Error(error.sqlMessage || error.message)
    }
  }

  public async editMovieById(id: string, editInformations: MovieEditDTO): Promise<void> {
    try {
      await this.getConnection()
        .update(editInformations)
        .from(MovieDatabase.TABLE_NAME)
        .where({ id })
    } catch (error) {
      throw new Error(error.sqlMessage || error.message)
    }
  }

  public async getMovies(filter?: MovieSearchFilterDTO): Promise<any> {
    try {
      if (filter) {
        const movies = await this.getConnection()
          .select('*')
          .from(MovieDatabase.TABLE_NAME)
          .where(filter)

        const mappedMovies = movies.map((movie: any) => ({
          id: movie.id,
          title: movie.title,
          director: movie.director,
          synopsis: movie.synopsis,
          rating: movie.rating,
          ratingsMade: movie.ratingsMade
        }))

        return { movies: mappedMovies, count: mappedMovies.length }
      } else {
        const movies = await this.getConnection()
          .select('*')
          .from(MovieDatabase.TABLE_NAME)

        const mappedMovies = movies.map((movie: any) => ({
          id: movie.id,
          title: movie.title,
          director: movie.director,
          synopsis: movie.synopsis,
          rating: movie.rating,
          ratingsMade: movie.ratingsMade
        }))

        return { movies: mappedMovies, count: mappedMovies.length }
      }
    } catch (error) {
      throw new Error(error.sqlMessage || error.message)
    }
  }
}
