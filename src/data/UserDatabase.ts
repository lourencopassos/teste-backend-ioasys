import { BaseDatabase } from './BaseDatabase'
import { User, UserEditDTO, UserRole } from '../model/User'

export class UserDatabase extends BaseDatabase {
  private static readonly TABLE_NAME = 'IoasysUsers'

  public async createUser(
    id: string,
    email: string,
    password: string,
    role: UserRole
  ): Promise<void> {
    try {
      await this.getConnection()
        .insert({
          id,
          email,
          password,
          role
        })
        .into(UserDatabase.TABLE_NAME)
    } catch (error) {
      throw new Error(error.sqlMessage || error.message)
    }
  }

  public async getUserByEmail(email: string): Promise<User> {
    try {
      const result = await this.getConnection()
        .select('*')
        .from(UserDatabase.TABLE_NAME)
        .where({ email })

      return User.toUserModel(result[0])
    } catch (error) {
      throw new Error(error.sqlMessage || error.message)
    }
  }

  public async getUserById(id: string): Promise<User> {
    try {
      const result = await this.getConnection()
        .select('*')
        .from(UserDatabase.TABLE_NAME)
        .where({ id })

      return User.toUserModel(result[0])
    } catch (error) {
      throw new Error(error.sqlMessage || error.message)
    }
  }

  public async softDeleteUserById(id: string): Promise<void> {
    await this.getConnection()
      .select('*')
      .update({ status: 'disabled' })
      .from(UserDatabase.TABLE_NAME)
      .where({ id })
  }

  public async editUserById(id: string, editInformations: UserEditDTO): Promise<void> {
    await this.getConnection()
      .update(editInformations)
      .from(UserDatabase.TABLE_NAME)
      .where({ id })
  }
}
