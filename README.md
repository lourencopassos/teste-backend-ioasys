# 📽️ API Node - Desafio Técnico Backend Node.Js

### Repositório do desafio técnico para desenvolvedor de Node.Js na empresa: Ioasys. Para saber mais sobre o escopo do desafio, [clique aqui](https://bitbucket.org/ioasys/teste-backend/src/master/).

## 🚀 Características do desafio

- Utilização de Node.Js
- Utilização de banco de dados SQL
- Utilização de Typescript

## 👨🏽‍💻 Tecnologias utilizadas
- Node.js
- Typescript
- MySQL
- Knex
- Express
- JwtToken
- Bcrypt
- Uuid
- MySQL Workbench

## 📝 Requisitos funcionais
Para uma visão completa das user stories do desafio, [clique aqui](https://bitbucket.org/ioasys/teste-backend/src/master/).

## 🚙 Instruções para rodar a aplicação

1. `npm install` para instalar todas as dependências;
2. `npm start` para rodar localmente o projeto.
3. `npm test` para rodar suíte de testes do projeto.


## 🔑 Acesso

```
role: user
email: user@ioasys.com
password: 123456

```

```
role: admin
email: admin@ioasys.com
password: 123456

```

## 🛤 Rotas da Aplicação
- Na API desenvolvida há rotas públicas e protegidas.
- Nas protegidas, há a necessidade de usar o header "Authorization:" token.
- O token tem duração de 90 minutos.

### 🔓 Rotas Públicas

<br>

- **Criação de usuário**

**`POST /user`** A rota deve receber um `email`, `password` e `role` dentro do corpo da requisição. O role pode ser `user` ou `admin`, para diferenciar o tipo de autorização do usuário. O `password` então é encriptado utilizando o `bcrypt` antes de ser gravado no banco de dados, assim como é gerado um id utilizando o `Uuid`. 

Exemplo de requisição:

```
{
  "email": "user@email.com",
  "password": "123456",
  "role": "user",
}
```

O usuário é gerado nativamente com o status `ENABLED` para controle de perfis ativos da aplicação e possibilitando um soft delete (onde não há exclusão do banco de dados)

- **Login de usuário**

**`POST /user/login`** A rota deve receber um `email` e `password` no corpo da requisição para a geração de token, que carrega o a liberação do usuário dentro da aplicação. A resposta deste é endpoint é o token.

Exemplo de requisição:

```
{
  "email": "user@email.com",
  "password": "123456"
}
```

Exemplo de resposta:

``` 
{
    "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2xlIjoiY2xpZW50IiwiaWF0IjoxNjAyOTg5ODk0LCJleH..."
}
```


### 🔐 Rotas Privadas

<br>

- **Deletar usuário (Desativação)**

**`PATCH /user/:id`** A rota deve receber o id através do _path variable_ e muda o `status` do usuário para `disabled`, sem excluir realmente o usuário do banco de dados. É necessário um token para a exclusão e apenas o role `admin` tem autorização para tal.


- **Avaliação de filmes**

**`PATCH /movie/:id`**  A rota recebe via _path variable_ o `id` do filme que será avaliado e antes verifica se aquele filme específico está disponível. Há também verificação de validade de `token`. Ainda há o envio de um _body_ com a chave `rating`, que deve ser um número entre 0 e 4.

Exemplo de corpo de requisição:

```
{
  "rating": 3,

}
```


- **Lista de filmes** 

**`GET /movie`** A rota verifica a validade do `token` e retorna a lista dos filmes que no banco de dados

Exemplo de resposta:

```
{
    "movies": [
        {
            "id": "5ad1fb20-d590-4aff-be04-350ee97920f0",
            "title": "Dunkirk",
            "director": "Christopher Nolan",
            "rating": 4,
            "ratingsMade": 1
        },
        {
            "id": "6d9e5ff7-1f11-44d5-a308-a8baacfa9ed9",
            "title": "O Irlandês",
            "director": "Martin Scorcese",
            "rating": 4,
            "ratingsMade": 1
        },
        {
            "id": "788cca3a-931f-4898-a209-65869ee7c5fb",
            "title": "A Ilha do Medo",
            "director": "Martin Scorcese",
            "rating": 4,
            "ratingsMade": 1
        },
        {
            "id": "ce3f7ab9-8963-437b-994c-02844acbdfc8",
            "title": "Dunkirk",
            "director": "Christopher Nolan",
            "rating": 4,
            "ratingsMade": 1
        }
    ]
}
```

- **Filtrar filmes por id** 

**`GET /movie/:id`** A rota verifica a validade do `token` e retorna o filme a partir do id recebido através do _path variable_.

Exemplo de resposta: 

```
{
    "movies": [
        {
            "id": "b35f550d-4f1a-4f01-97d1-9e999265de13",
            "title": "Us",
            "director": "Jordan Peele",
            "rating": 4,
            "ratingsMade": 1
        }
    ]
}
```

- **Criar filme**

**`POST /movie/create`** A rota verifica a validade do `token` e recebe no _body_ as informações do filme a ser criado: `title`,`director` e `synopsis`. `ratings made` é nativamente 0 e a rating inicial é `null`. 

Exemplo de Requisição

```
{
    "title": "Batman Begins",
    "director": "Christopher Nolan",
    "synopsis": "good movie"
}
```

## 🗄️ Banco de dados

Tabela de usuários:

```
CREATE TABLE IoasysUsers (
  id VARCHAR(255) PRIMARY KEY,
  email VARCHAR(255) NOT NULL,
  password VARCHAR(255) NOT NULL,
  role ENUM ("admin", "user") NOT NULL,
  status ENUM ("enabled", "disabled") NOT NULL
); 
```

Tabela de filmes

```

CREATE TABLE IoasysMovies (
  id VARCHAR(255) PRIMARY KEY,
  title VARCHAR(255) NOT NULL,
  synopsis VARCHAR(255) NOT NULL,
  director VARCHAR(255) NOT NULL,
  rating FLOAT,
  ratingsMade INT DEFAULT 0
); 

```





#### 👋🏽 Contato

Lourenço Passos | Desenvolvedor Web Fullstack | lo.passos93@gmail.com | 51-996106010




